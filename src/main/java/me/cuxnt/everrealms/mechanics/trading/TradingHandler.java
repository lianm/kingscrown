package me.cuxnt.everrealms.mechanics.trading;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TradingHandler {

    public static ItemStack confirmButton(boolean accepted){
        ItemStack button;


        if(accepted){
            button = new ItemStack(Material.INK_SACK, 1, (short) 10);
        }else {
            button = new ItemStack(Material.INK_SACK, 1, (short) 8);
        }
        ItemMeta buttonMeta = button.getItemMeta();
        buttonMeta.setDisplayName(ChatColor.YELLOW + "Click to ACCEPT Trade");
        List lore =  new ArrayList<String>();
        lore.add(ChatColor.GRAY.toString() + "Modify the trade to unaccept.");
        buttonMeta.setLore(lore);
        button.setItemMeta(buttonMeta);
        return button;
    }
}
