package me.cuxnt.everrealms.mechanics.abilities;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJump implements Listener {

    public void onEnable() {
        Console.log(this.getClass(), true);
        Bukkit.getPluginManager().registerEvents( this, KingsCrown.plugin);
    }

    @EventHandler
    public void onFlightAttempt(PlayerToggleFlightEvent event) {
        if(event.getPlayer().isOp()) return;
        Player p = event.getPlayer();
        p.sendMessage(ChatColor.GREEN + "**WOOSH**");
        p.playSound(p.getLocation(), Sound.ENTITY_SHEEP_DEATH, 10, -10);
        event.setCancelled(true);
        Vector v = p.getLocation().getDirection().multiply(1).setY(1);
        p.setVelocity(v);

    }
}
