package me.cuxnt.everrealms.mechanics.ranks;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public enum Rank {

    DEFAULT(ChatColor.WHITE, ""),
    OWNER(ChatColor.RED, "OWNER"),
    DEVELOPER(ChatColor.RED, "DEV"),
    BUILDER(ChatColor.GRAY, "B"),
    SUBSCRIBER(ChatColor.GREEN, "SUB"),
    MANAGER(ChatColor.YELLOW, "MANAGER"),
    GM(ChatColor.AQUA, "GM"),
    PMOD(ChatColor.WHITE, "PMOD");

    private String rank_id;
    private String chat_tag;

    Rank(ChatColor color, String rank_id){
        this.rank_id = rank_id;
        this.chat_tag = color + rank_id;
    }

    public String getRankId() {
        return rank_id;
    }

    public String getChatTag() {
        return chat_tag;
    }

    public boolean isStaff(){
        switch (this){
            case GM:
            case OWNER:
                return true;
        }
        return false;
    }

    public static String toList(){
        String list_stirng = "";
        for(Rank rank : Rank.values()){
            if(rank.ordinal() == Rank.values().length){
                //Adds full stop to end
                list_stirng.concat(rank + ".");
            }else {
                list_stirng.concat(rank + ", ");
            }
        }
        return list_stirng;
    }

    public static Rank getRank(String rank_string){
        return Rank.valueOf(rank_string);
    }
}
