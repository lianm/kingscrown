package me.cuxnt.everrealms.mechanics.ranks;

import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args[0].equals("set")){
            Player player_target = Bukkit.getPlayer(args[1]);
            PlayerStats stats = PlayerStats.playerdata.get(player_target);
            Rank rank = Rank.valueOf(args[0].toUpperCase());
            stats.setRank(rank);
            player_target.sendMessage("Your rank has been set to " + rank.getChatTag());
        }
        else if(PlayerStats.playerdata.containsKey(Bukkit.getPlayer(args[0]))){
            PlayerStats playerStats = PlayerStats.playerdata.get(Bukkit.getPlayer(args[0]));
            if(sender instanceof Player){
                sender.sendMessage(playerStats.getName() + "'s rank is " + playerStats.getRank());
            }
        }else{
            sender.sendMessage("Ranks: " + Rank.toList());
            sender.sendMessage("/rank set <player> <rank> -- set <player>'s rank to <rank>.");
            sender.sendMessage("/rank <player> -- View <player>'s rank." );
        }
        return false;
    }
}
