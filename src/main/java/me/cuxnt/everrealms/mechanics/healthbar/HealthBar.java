package me.cuxnt.everrealms.mechanics.healthbar;

import me.cuxnt.everrealms.KingsCrown;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

public class HealthBar {

    private BossBar bar;
    private int taskId = -1;

    public BossBar getBar() {
        return bar;
    }

    public void addPlayer(Player player){
        bar.addPlayer(player);
    }

    public void createBar(){
        bar = Bukkit.createBossBar(ChatColor.RED + "HP: ", BarColor.RED, BarStyle.SOLID);
        bar.setVisible(true);
    }
}
