package me.cuxnt.everrealms.mechanics.healthbar;

import me.cuxnt.everrealms.KingsCrown;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class HealthBarListener implements Listener {

    HealthBar healthBar;

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        healthBar = new HealthBar();
        healthBar.createBar();
        new BukkitRunnable(){
            @Override
            public void run() {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    double percent = ((100 / player.getMaxHealth()) * player.getHealth()) /100;
                    healthBar.getBar().setTitle(ChatColor.RED + "HP: " + player.getHealth() + "/" + player.getMaxHealth());
                    healthBar.getBar().setProgress((float) percent);
                }
            }
        }.runTaskTimerAsynchronously(KingsCrown.plugin, 20,20);
    }
}
