package me.cuxnt.everrealms.mechanics.tiers;

import me.cuxnt.everrealms.mechanics.gear.GearType;
import me.cuxnt.everrealms.mechanics.gear.Item;
import org.bukkit.ChatColor;
import org.bukkit.Material;

public enum Tier {

    ONE("Broken", ChatColor.WHITE, "WOOD", "LEATHER"),
    TWO("Old", ChatColor.GREEN, "STONE", "CHAINMAIL"),
    THREE("Magical", ChatColor.AQUA , "IRON", "IRON"),
    FOUR("Ancient", ChatColor.LIGHT_PURPLE, "DIAMOND", "DIAMOND"),
    FIVE("Legendary", ChatColor.YELLOW, "GOLD", "GOLD");

    private String wep;
    private String armor;
    private ChatColor color;
    private int tier;
    private String prefix;

    Tier(String prefix, ChatColor color, String wep, String armor) {
        this.tier = this.ordinal();
        this.wep = wep;
        this.color = color;
        this.prefix = prefix;
        this.armor = armor;
    }

    public static Tier getTierFromItem(Material material){
        for(Tier tier : Tier.values()){
            if(material.toString().contains(tier.getArmor()) ||
                    material.toString().contains(tier.getWep())){
                return tier;
            }
        }
        return null;
    }

    public int index() {
        return tier;
    }

    public String getArmor() {
        return armor;
    }

    public String getWep() {
        return wep;
    }

    public String getPrefix(){
        return prefix;
    }

    public String getMaterialPrefix(Item item){
        if(item.getGearType().equals(GearType.ARMOR))
            return getArmor();
        else
            return getWep();
    }

    public ChatColor getColor() {
        return color;
    }

}
