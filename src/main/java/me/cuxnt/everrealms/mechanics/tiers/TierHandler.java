package me.cuxnt.everrealms.mechanics.tiers;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

public class TierHandler implements Listener {

    public static int getStringTier(String s){
        //Check for enchanted items
        if(s.contains("[")){
            s = s.split("]")[1];
        }
        //Find matching color
        for (Tier tier : Tier.values()){
            if(s.contains(tier.getColor().toString())){
                return tier.index();
            }
        }
        return 0;
    }

    public static int getItemTier(ItemStack item){
        return getStringTier(item.getItemMeta().getDisplayName());
    }

    public static int getHighestTierArmor(LivingEntity entity) {
        int tier = 0;
        for (ItemStack item : entity.getEquipment().getArmorContents()) {
            if (tier < getStringTier(item.getItemMeta().getDisplayName()))
                tier = getStringTier(item.getItemMeta().getDisplayName());
        }
        return tier;
    }
}
