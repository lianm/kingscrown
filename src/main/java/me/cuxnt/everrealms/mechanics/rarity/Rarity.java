package me.cuxnt.everrealms.mechanics.rarity;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public enum Rarity {

    COMMON(ChatColor.GRAY),
    UNCOMMON(ChatColor.GREEN),
    RARE(ChatColor.AQUA),
    EPIC(ChatColor.DARK_PURPLE),
    LEGENDARY(ChatColor.YELLOW);

    private ChatColor color;
    private String title;
    private int id;

    Rarity(ChatColor color) {
        this.color = color;
        this.id = this.ordinal();
        this.title = color + StringUtils.capitalize(this.toString().toLowerCase());
    }

    public String getTitle() {
        return title;
    }

    public int toInt() {
        return id;
    }

    public static Rarity getRarity(ItemStack itemStack){
        if(itemStack.hasItemMeta()) {
            for (String line : itemStack.getItemMeta().getLore()) {
                for (Rarity rarity : Rarity.values()) {
                    line = ChatColor.stripColor(line).toLowerCase();
                    if (line.contains(rarity.getTitle())) {
                        return rarity;
                    }
                }
            }
        }
        return Rarity.COMMON;
    }
}
