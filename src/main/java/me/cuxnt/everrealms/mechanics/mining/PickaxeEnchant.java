package me.cuxnt.everrealms.mechanics.mining;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

public enum PickaxeEnchant {

    DOUBLE_ORE(20),
    TRIPLE_ORE(10),
    GEM_FIND(5);

    private int max_level;

    PickaxeEnchant(int max_level){
        this.max_level = max_level;
    }

    public static String createLine(PickaxeEnchant ench, int level){
        String line = ench.toString().replace("_", " ");
        line = ChatColor.RED + line + ": " + level + "%";
        return line;
    }

//    public static HashMap[] stripEnchants(ItemStack pickaxe){
//        HashMap[] maps;
//        HashMap<PickaxeEnchant, Integer> enchant = new HashMap<>();
//        List lore = pickaxe.getItemMeta().getLore();
//        int lore_size = 3;
//        //Pickaxe lore size without enchants = 3 lines
//        if(lore.size() > lore_size){
//            for(int i = lore_size-1; i < (lore.size() - lore_size); i++){
//                String line = lore.get(i).toString();
//                line = ChatColor.stripColor(line);
//                String enchName = line.split(": ")[0].replace(" ", "_");
//                String enchLevel = line.split(": ")[1].replace("%", "");
//                enchant.put(PickaxeEnchant.valueOf(enchName.toUpperCase()), Integer.parseInt(enchLevel));
//            }
//
//        }
//        return enchant;
//    }

}
