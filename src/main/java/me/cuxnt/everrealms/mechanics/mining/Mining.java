package me.cuxnt.everrealms.mechanics.mining;

import me.cuxnt.everrealms.KingsCrown;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class Mining implements Listener {

    public void onEnable(){
        System.out.println("[Mining] has been enabled!");
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
    }

    public void onDisbale(){
        System.out.println("[Mining] has been disabled");
    }

}
