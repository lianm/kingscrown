package me.cuxnt.everrealms.mechanics.mining.ore;

import me.cuxnt.everrealms.mechanics.gear.Item;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public enum Ores {

    DIAMOND(Material.DIAMOND, "Sparkling Ancient Ore"),
    GOLD(Material.GOLD_INGOT, "Rich Gold Ore");

    private Material material;
    private Material ore;
    private String title;

    Ores(Material material, String title){
        this.material = material;
        this.title = title;
        this.ore = Material.getMaterial(this.toString() + "_ORE");
    }

    public ItemStack create(){
        ItemStack ore = new ItemStack(material);
        ItemMeta oreMeta = ore.getItemMeta();
        oreMeta.setDisplayName(title);
        List lore = new ArrayList();
        lore.add(ChatColor.GRAY + "Ore mined from the kings island.");
        oreMeta.setLore(lore);
        ore.setItemMeta(oreMeta);
        return ore;
    }

    public static Ores getOre(Block ore_mined){
        for(Ores ore : Ores.values()){
            Material type = Material.getMaterial(ore_mined.getType().toString().replace("LEGACY_",""));
            System.out.println(type);
            if(ore.ore.equals(type)){
                return ore;
            }
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    public Material getMaterial() {
        return material;
    }
}
