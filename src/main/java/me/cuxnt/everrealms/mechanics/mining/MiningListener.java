package me.cuxnt.everrealms.mechanics.mining;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.mining.ore.Ores;
import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.HashMap;

public class MiningListener implements Listener {

    private HashMap<Location, Material> blockMined = new HashMap<>();

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        Console.log(this.getClass(), true);
    }

    public void onDisable(){
        for(Location location : blockMined.keySet()){
            location.getBlock().setType(blockMined.get(location));
        }

        Console.log(this.getClass(), false);
    }


    @EventHandler
    public void oreMine(BlockBreakEvent event){
        event.setCancelled(true);
        Block block = event.getBlock();
        Player player = event.getPlayer();
        if(Ores.getOre(block) != null){
            //MiningStats.addOre(player, 1); // Updates the amount to the playerstats hashmap
            Ores ore  = Ores.getOre(block);
            player.getInventory().addItem(ore.create());
            PlayerStats playerStats = PlayerStats.playerdata.get(player);
            playerStats.getMiningStats().setOresMined(playerStats.getMiningStats().getOresMined()+1);
            blockMined.put(event.getBlock().getLocation(), block.getType());
            block.setType(Material.STONE);
            Bukkit.getScheduler().runTaskLater(KingsCrown.plugin, new Runnable() {
                @Override
                public void run() {
                    block.setType(blockMined.get(block.getLocation()));
                    blockMined.remove(block.getLocation());
                }
            }, 20 * 10);
        }


    }


}
