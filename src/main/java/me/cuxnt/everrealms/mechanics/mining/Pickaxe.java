package me.cuxnt.everrealms.mechanics.mining;

import me.cuxnt.everrealms.mechanics.tiers.Tier;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Pickaxe {

    private int level;
    private int exp;
    private int maxexp;
    private int tier;
    private PickaxeEnchant[] enchants;

    String level_prefix = "Level: ";
    String exp_prefix = "EXP: ";

    Pickaxe(ItemStack item){
        List lore =  item.getItemMeta().getLore();
        for (Object line : lore) {
            line = ChatColor.stripColor(line.toString());

            if(line.toString().contains(level_prefix)){
                this.level = Integer.parseInt(line.toString().split(level_prefix)[1]);
            }

        }

        if(level % 20 == 0) this.tier = Math.round(level / 20);

    }

    Pickaxe(){
        this.level = 1;
        this.tier = 1;
        this.exp = 0;
        this.maxexp = 100;
        this.enchants = null;
    }

    public ItemStack create(){
        ItemStack pickaxe = new ItemStack(Material.getMaterial(Tier.values()[tier].getWep() + "_PICKAXE"));
        ItemMeta pickMeta = pickaxe.getItemMeta();
        List lore = new ArrayList();
        lore.add(ChatColor.WHITE + level_prefix + level);
        lore.add(ChatColor.WHITE + exp_prefix + exp + " / " + maxexp);
        for(PickaxeEnchant ench : enchants){
            lore.add(PickaxeEnchant.createLine(ench, level));
        }
        return pickaxe;
    }

    public int getTier() {
        return tier;
    }

    public int getExp() {
        return exp;
    }

    public int getLevel() {
        return level;
    }

    public int getMaxexp() {
        return maxexp;
    }

    public PickaxeEnchant[] getEnchants() {
        return enchants;
    }
}
