package me.cuxnt.everrealms.mechanics.donations;

import org.apache.commons.lang.StringUtils;

public enum DonationType {
    SUBSCRIBER(5.95);

    private double price;
    private String name;

    DonationType(double price){
        this.price = price;
        this.name = StringUtils.capitalize(this.toString());
    }
}
