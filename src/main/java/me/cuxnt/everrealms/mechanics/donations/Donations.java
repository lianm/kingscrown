package me.cuxnt.everrealms.mechanics.donations;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class Donations {

    private String uuid;
    private int price;
    private DonationType donationType;

    Donations(OfflinePlayer player, DonationType donationType){
        this.uuid = player.getUniqueId().toString();
        this.donationType = donationType;
    }
}
