package me.cuxnt.everrealms.mechanics.lootcrate;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.warzone.Warzone;
import me.cuxnt.everrealms.mechanics.warzone.WarzoneHandler;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.HashMap;
import java.util.Random;

public class LootCrate {

    public static HashMap<Location, Material> lootcrateInstance = new HashMap<>();
    private final int lootcrate_amt = 15;
    private int current_amt = 0;
    public void onEnable(){

        Bukkit.getScheduler().runTaskLater(KingsCrown.plugin, new Runnable() {

            @Override
            public void run() {
                if(Warzone.playerInWarzone.keySet().size() > 0) {
                    while (current_amt < lootcrate_amt) {
                        current_amt++;
                        setCrate();
                    }
                }
            }
        }, 600);

    }

    public void onDisable(){
        for(Location location : LootCrate.lootcrateInstance.keySet()){
            remove(location);
        }
        Console.log(this.getClass(), false);
    }

    public static void setCrate(){
        Location location = randomCrateLocation();
        Block previous_block = location.getWorld().getBlockAt(location);
        lootcrateInstance.put(location, previous_block.getType());
        previous_block.setType(Material.CHEST);
        Bukkit.getScheduler().runTaskLater(KingsCrown.plugin, new Runnable() {
            @Override
            public void run() {
                remove(location);
                setCrate();
            }
        }, 20 * 30);
    }

    static void remove(Location location){
        Block previous_block = location.getBlock();
        previous_block.setType(Material.AIR);
        lootcrateInstance.remove(location);
    }

    static Location randomCrateLocation(){
        Location startFrom = Warzone.warzone_origin;
        Random random = new Random();
        Location output = new Location(startFrom.getWorld(),
                (random.nextInt(WarzoneHandler.radius) - random.nextInt(WarzoneHandler.radius)),
                (startFrom.getWorld().getHighestBlockYAt(startFrom) + 1),
                (random.nextInt(WarzoneHandler.radius) - random.nextInt(WarzoneHandler.radius)));
        return output;
    }
}
