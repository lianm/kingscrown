package me.cuxnt.everrealms.mechanics.combust;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.trading.TradingHandler;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Combust implements Listener {

    public static final String inventory_name = "Combustion Inputs";

    public void onEnable(){
        Console.log(this.getClass(), true);
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
    }

    public static void openCombustionGUI(Player player){
        Inventory inventory = Bukkit.createInventory(player, InventoryType.HOPPER, inventory_name);
        inventory.setItem(4, TradingHandler.confirmButton(false));
        player.openInventory(inventory);
    }

    @EventHandler
    public void combustAccept(InventoryClickEvent event){
        if(event.getCurrentItem().getType().equals(Material.INK_SACK)
                && (event.getClickedInventory().getTitle() == Combust.inventory_name)){
            List<ItemStack> item_list = new ArrayList<>();
            for(int i = 0; i < 4; i++){
                Inventory inventory = event.getInventory();
                ItemStack item = inventory.getItem(i);
                item_list.add(item);
                event.getWhoClicked().sendMessage(" --> " + item.getItemMeta().getDisplayName() + " has been added!");
            }
            event.setCurrentItem(TradingHandler.confirmButton(true));
            event.getWhoClicked().closeInventory();
        }
    }

    public void combustItems(Player player, List<ItemStack> items){
        Gear gear = new Gear(items.get(0));
        gear.setRarity(Rarity.values()[gear.getRarity().ordinal()+1]);
        ItemStack result = gear.build();
        player.sendMessage(result.getItemMeta().getDisplayName() + " has been created, and added into you're inventory.");
        player.getInventory().addItem(result);
    }

    public void refund(Player p, ItemStack[] items){
        for(ItemStack old_item : items) {
            p.getInventory().addItem(old_item);
        }
    }

}
