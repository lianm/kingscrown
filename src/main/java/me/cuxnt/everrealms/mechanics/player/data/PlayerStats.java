package me.cuxnt.everrealms.mechanics.player.data;

import me.cuxnt.everrealms.mechanics.donations.Donations;
import me.cuxnt.everrealms.mechanics.ranks.Rank;
import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerStats {

    public static HashMap<Player, PlayerStats> playerdata = new HashMap<>();

    private String uuid;
    private String name;
    private String rank;
    private String ip;
    private Donations donations;
    private boolean banned;
    private boolean muted;
    private CombatStats combat_stats;
    private MiningStats mining_stats;

    //Get player instance from data base
    public PlayerStats(Player player) {
        this.uuid = player.getUniqueId().toString();
        this.name = player.getName();
        this.ip = player.getAddress().getAddress().toString();
        this.rank = Rank.DEFAULT.getRankId();
        this.combat_stats = new CombatStats();
        this.mining_stats = new MiningStats();
    }

    public PlayerStats(String uuid){
        FileConfiguration playerConfig = PlayerConfig.get();
        this.uuid = uuid;
        this.rank = playerConfig.getString(uuid + ".rank");
        this.banned = playerConfig.getBoolean(uuid + ".banned");
        this.muted = playerConfig.getBoolean(uuid + ".muted");
        this.ip = playerConfig.getString(uuid + ".ip");
        this.combat_stats = new CombatStats(playerConfig, uuid);
        this.mining_stats = new MiningStats(playerConfig, uuid);
    }

    public String getName() {
        return name;
    }

    public CombatStats getCombatStats() {
        return combat_stats;
    }

    public Donations getDonations() {
        return donations;
    }

    public MiningStats getMiningStats() {
        return mining_stats;
    }

    public Rank getRank() {
        return Rank.valueOf(rank);
    }

    public void setRank(Rank rank) {
        this.rank = rank.getRankId();
    }

    public String getIp() {
        return ip;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isBanned() {
        return banned;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMiningStats(MiningStats mining_stats) {
        this.mining_stats = mining_stats;
    }

    public void setCombatStats(CombatStats combat_stats) {
        this.combat_stats = combat_stats;
    }

    public static void sync(Player player){
        String uuid = player.getUniqueId().toString();
        PlayerStats playerStats = playerdata.get(player);
        FileConfiguration playerConfig = PlayerConfig.get();
        playerConfig.set(uuid + ".rank", playerStats.getRank().getRankId());
        playerConfig.set(uuid + ".banned", playerStats.isBanned());
        playerConfig.set(uuid + ".muted", playerStats.isMuted());
        playerConfig.set(uuid + ".combatstats.kills", playerStats.getCombatStats().getKills());
        playerConfig.set(uuid + ".combatstats.deaths", playerStats.getCombatStats().getDeaths());
        playerConfig.set(uuid + ".combatstats.streak", playerStats.getCombatStats().getStreak());
        playerConfig.set(uuid + ".combatstats.maxhp", playerStats.getCombatStats().getMaxhp());
        playerConfig.set(uuid + ".miningstats.ore_mined", playerStats.getMiningStats().getOresMined());
        playerConfig.set(uuid + ".miningstats.enchants.double_ore", playerStats.getMiningStats().getDouble_ore());
        playerConfig.set(uuid + ".miningstats.enchants.item_find", playerStats.getMiningStats().getItem_find());
        playerConfig.set(uuid + ".miningstats.enchants.speed", playerStats.getMiningStats().getSpeed());
        player.sendMessage(ChatColor.GREEN + "Your data from the volcano has been saved safely.");
        PlayerConfig.save();
    }

    public static void load(Player player){
        PlayerStats playerStats;
        if(PlayerConfig.get().contains(player.getUniqueId().toString())) {
            playerStats = new PlayerStats(player.getUniqueId().toString());
        }else{
            playerStats = new PlayerStats(player);
        }
         playerdata.put(player, playerStats);
        player.sendMessage(ChatColor.GREEN + "Your data has been successfully loaded to the volcano.");
    }
}