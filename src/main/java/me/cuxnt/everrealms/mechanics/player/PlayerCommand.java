package me.cuxnt.everrealms.mechanics.player;

import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import me.cuxnt.everrealms.mechanics.ranks.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player target = Bukkit.getPlayer(args[0]);
        if(target == null){
            target.sendMessage(ChatColor.RED + "Use the players name: /player <name>");
        }

        String main_arg = args[1];
        PlayerStats playerStats = PlayerStats.playerdata.get(target);
        if(main_arg.equals("setrank")){
            Rank rank = Rank.valueOf(args[2]);
            playerStats.setRank(rank);
            sender.sendMessage(target.getName() + "'s rank has been set to " + rank.getChatTag());
        }else if(main_arg.equals("sync")){
            PlayerStats.sync(target);
        }else if(main_arg.equals("stats")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                player.sendMessage("Rank: " + playerStats.getRank());
                player.sendMessage("Muted: " + playerStats.isMuted());
                player.sendMessage("Banned: " + playerStats.isBanned());
                player.sendMessage("Combat Stats -");
                player.sendMessage("Kills: " + playerStats.getCombatStats().getKills());
                player.sendMessage("Deaths: " + playerStats.getCombatStats().getDeaths());
                player.sendMessage("Highest Kill Streak: " + playerStats.getCombatStats().getStreak());
                player.sendMessage("Max Health: " + playerStats.getCombatStats().getMaxhp());
                player.sendMessage("Mining Stats -");
                player.sendMessage("Mined Ore: " + playerStats.getMiningStats().getOresMined());
                player.sendMessage("Double Ore: " + playerStats.getMiningStats().getDouble_ore());
                player.sendMessage("Item Find: " + playerStats.getMiningStats().getItem_find());
                player.sendMessage("Speed: " + playerStats.getMiningStats().getSpeed());
            }
        }else if(playerStats.getRank().isStaff()){
            if(main_arg.equals("ban")){
                boolean banned = Boolean.parseBoolean(args[1]);
                if(banned){
                    playerStats.setBanned(true);
                    target.sendMessage(ChatColor.RED  + "You have been muted.");
                }else{
                    playerStats.setBanned(false);
                }
            }else if(main_arg.equals("mute")){
                boolean muted = Boolean.parseBoolean(args[1]);
                if(muted){
                    playerStats.setMuted(true);
                    target.kickPlayer(ChatColor.RED  + "You have been banned.");
                }else{
                    playerStats.setMuted(false);
                }
            }
        }
        return false;
    }
}
