package me.cuxnt.everrealms.mechanics.player.data;

import org.bson.Document;
import org.bukkit.configuration.file.FileConfiguration;

public class CombatStats{


    private int maxhp;
    private int kills;
    private int deaths;
    private int streak;

    public CombatStats(){
        this.kills = 0;
        this.deaths = 0;
        this.streak = 0;
        this.maxhp = 0;
    }

    public CombatStats(PlayerStats stats){
        this.maxhp = stats.getCombatStats().getMaxhp();
        this.kills= stats.getCombatStats().getKills();
        this.deaths = stats.getCombatStats().getDeaths();
        this.streak = stats.getCombatStats().getStreak();
    }

    public CombatStats(FileConfiguration playerConfig, String uuid){
        this.kills = playerConfig.getInt(uuid + ".combatstats.kills");
        this.deaths = playerConfig.getInt(uuid + ".combatstats.deaths");
        this.streak = playerConfig.getInt(uuid + ".combatstats.streak");
        this.maxhp = playerConfig.getInt(uuid + ".combatstats.maxhp");
    }

    public int getDeaths() {
        return deaths;
    }

    public int getKills() {
        return kills;
    }

    public int getMaxhp() {
        return maxhp;
    }

    public int getStreak() {
        return streak;
    }

    public void setDeaths(int deaths) {
        this.deaths += deaths;
    }

    public void setKills(int kills) {
        this.kills += 1;
    }

    public void setMaxhp(int maxhp) {
        this.maxhp = maxhp;
    }

    public void setHighestKillStreak(int streak) {
        this.streak += streak;
    }

}

