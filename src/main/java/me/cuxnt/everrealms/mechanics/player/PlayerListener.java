package me.cuxnt.everrealms.mechanics.player;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.kits.Kit;
import me.cuxnt.everrealms.mechanics.kits.KitHandler;
import me.cuxnt.everrealms.mechanics.player.data.PlayerConfig;
import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import me.cuxnt.everrealms.utils.StringUtil;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerListener implements Listener {

    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        Console.log(this.getClass(), true);

        PlayerConfig.setup();

        //load players data to hashmap on reload
        for (Player p : Bukkit.getOnlinePlayers()) {
            PlayerStats.load(p);
        }
    }

    public void onDisbale() {
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        Console.log(this.getClass(), false);

        //load players data to hashmap on reload
        for (Player p : PlayerStats.playerdata.keySet()) {
            PlayerStats.sync(p);
        }
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        event.setJoinMessage(null);

        StringUtil.sendCenteredMessage(player, "");
        StringUtil.sendCenteredMessage(player, "" + ChatColor.GOLD + ChatColor.BOLD + "Kings Crown v" + KingsCrown.version);
        StringUtil.sendCenteredMessage(player, "");
        StringUtil.sendCenteredMessage(player, ChatColor.GRAY + "Welcome to play.kingscrown.net");
        StringUtil.sendCenteredMessage(player, ChatColor.GRAY + "This server is a Dungeon Realms style pvp server,");
        StringUtil.sendCenteredMessage(player, ChatColor.GRAY + "type /help to learn more.");
        StringUtil.sendCenteredMessage(player, "");

        //Register player to hashmap and set-up player if new
        PlayerStats.load(player);

        //Set players health and health scale
        PlayerHandler.hpCheck(player);
    }

    @EventHandler
    public void antiBuild(BlockPlaceEvent event){
        if(event.getPlayer().getGameMode().equals(GameMode.SURVIVAL)){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void respawnPlayer(PlayerRespawnEvent event){
        Player player = event.getPlayer();
        player.sendMessage("Respawning player");
        player.teleport(KingsCrown.spawn);
        Kit kit = KitHandler.kitDefault();
    }
}
