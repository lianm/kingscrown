package me.cuxnt.everrealms.mechanics.player.data;

import org.bson.Document;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class MiningStats {

    private int double_ore;
    private int item_find;
    private int speed;
    private int ores_mined;

    public MiningStats(){
        this.double_ore = 0;
        this.speed = 0;
        this.item_find = 0;
        this.ores_mined = 0;
    }

    public MiningStats(PlayerStats playerStats){
        this.ores_mined = playerStats.getMiningStats().getOresMined();
        this.double_ore = playerStats.getMiningStats().getDouble_ore();
        this.item_find = playerStats.getMiningStats().getItem_find();
        this.speed = playerStats.getMiningStats().getSpeed();
    }

    public MiningStats(FileConfiguration playerConfig, String uuid){
        this.ores_mined = playerConfig.getInt(uuid + ".miningstats.ore_mined");
        this.double_ore = playerConfig.getInt(uuid + ".miningstats.enchants.double_ore");
        this.item_find = playerConfig.getInt(uuid + ".miningstats.enchants.item_find");
        this.speed = playerConfig.getInt(uuid + ".miningstats.enchants.speed");
    }


    public int getDouble_ore() {
        return double_ore;
    }

    public int getItem_find() {
        return item_find;
    }

    public int getSpeed() {
        return speed;
    }

    public int getOresMined() {
        return ores_mined;
    }

    public void setOresMined(int ores_mined) {
        this.ores_mined = ores_mined;
    }

    @Override
    public String toString() {
        return "" + ores_mined;
    }

    public static void addOre(Player player, int amt){
        PlayerStats playerStats = PlayerStats.playerdata.get(player);
        //TODO get Mining Stats - returns null
        MiningStats miningStats = playerStats.getMiningStats();
        miningStats.setOresMined(miningStats.getOresMined() + amt);
        playerStats.setMiningStats(miningStats);
        PlayerStats.playerdata.remove(player);
        player.sendMessage("Ore has been added to the data base");
    }

    public void setDoubleOre(int double_ore) {
        this.double_ore = double_ore;
    }

    public void setItemFind(int item_find) {
        this.item_find = item_find;
    }

    public void setMiningSpeed(int speed) {
        this.speed = speed;
    }
}
