package me.cuxnt.everrealms.mechanics.player;

import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.gear.GearHandler;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class PlayerHandler{

    public static void hpCheck(Player p) {

        PlayerInventory i = p.getInventory();
        double base_hp = 50.0;
        double health = base_hp;
        double vit = 0.0;

        List<Gear> gear_list = GearHandler.getGearList(p);
        for(Gear gear : gear_list){
            health += gear.getStats().getHp();
        }

        if (vit > 0.0) {
            double mod = vit * 0.05;
            health += health * (mod / 100.0);

        }
        p.setMaxHealth(health);
        p.setHealthScale(20.0);
        p.setHealthScaled(true);
    }
}
