package me.cuxnt.everrealms.mechanics.kits;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.gear.Item;
import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import java.util.Random;

public class KitHandler implements Listener {

    public void onEnable(){
        Console.log(this.getClass(), true);
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
    }

    private static Kit kit_default;

    public static Kit kitDefault(){
        kit_default = new Kit("default");
        Tier tier = Tier.FOUR;
        Rarity rarity = Rarity.COMMON;
        kit_default.setHelm(new Gear(tier, Item.BOOTS, rarity).build());
        kit_default.setChest(new Gear(tier, Item.BOOTS, rarity).build());
        kit_default.setLegs(new Gear(tier, Item.BOOTS, rarity).build());
        kit_default.setBoots(new Gear(tier, Item.BOOTS, rarity).build());
        kit_default.setWep(new Gear(tier, Item.values()[new Random().nextInt(4)], rarity).build());
        return kit_default;
    }


}
