package me.cuxnt.everrealms.mechanics.kits;

import org.bukkit.inventory.ItemStack;

public class Kit {

    private String id;
    private ItemStack wep;
    private ItemStack helm;
    private ItemStack chest;
    private ItemStack legs;
    private ItemStack boots;

    Kit(String id){
        this.id = id;
        this.wep = wep;
        this.helm = helm;
        this.chest = chest;
        this.legs = legs;
    }

    public ItemStack getChest() {
        return chest;
    }

    public ItemStack getWep() {
        return wep;
    }

    public String getId() {
        return id;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public ItemStack getHelm() {
        return helm;
    }

    public ItemStack getLegs() {
        return legs;
    }

    public void setChest(ItemStack chest) {
        this.chest = chest;
    }

    public void setHelm(ItemStack helm) {
        this.helm = helm;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setWep(ItemStack wep) {
        this.wep = wep;
    }

    public void setBoots(ItemStack boots) {
        this.boots = boots;
    }

    public void setLegs(ItemStack legs) {
        this.legs = legs;
    }


}
