package me.cuxnt.everrealms.mechanics.crafting;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.gear.Item;
import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import xyz.xenondevs.particle.ParticleEffect;

public class CraftingListener implements Listener {

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        new BukkitRunnable(){
            @Override
            public void run() {
                for(Player player : Bukkit.getOnlinePlayers()){
                    ParticleEffect.CAMPFIRE_SIGNAL_SMOKE.display(player.getLocation(), new Vector(1, 0, 1), 1f, 0, null, Bukkit.getOnlinePlayers());
                }
            }
        }.runTaskTimerAsynchronously(KingsCrown.plugin, 0, 20);
    }

    @EventHandler
    public void customCraft(CraftItemEvent event){
        ItemStack result = event.getInventory().getResult();
        Item item = Item.getItem(result);
        Rarity rarity = Rarity.COMMON;
        Tier tier = Tier.getTierFromItem(result.getType());

        //Debug
        System.out.println(tier.toString() + " " + rarity.getTitle() + item.getTitle());

        event.setCancelled(true);
        event.getWhoClicked().closeInventory();
        Gear gear_result = new Gear(tier, item, rarity);
        event.getWhoClicked().getInventory().addItem(gear_result.build());
        ParticleEffect.FLAME.display(event.getWhoClicked().getLocation());

    }
}
