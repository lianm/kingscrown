package me.cuxnt.everrealms.mechanics.damage;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import javax.swing.text.html.parser.Entity;

public class Damage implements Listener {

    public void onEnable(){
        Console.log(this.getClass(), true);
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
    }

    //Set Bonus Damage for Velocity
    @EventHandler
    public void setVelocityDamage(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            Player damager = (Player) e.getDamager();
            Player victim = (Player) e.getEntity();
            int damager_y = damager.getLocation().getBlockY();
            double damage = e.getDamage();
            damager_y /= 10;
            double bonus_damage = Math.pow(1.5, damager_y) /2;
            e.setDamage(damage *= bonus_damage);
        }
    }

    public static double getBonus(int level){
        System.out.println("Level: " + level);
        level /= 10;
        double bonus_damage = Math.pow(1.5, level) /2;
        return bonus_damage;
    }
}
