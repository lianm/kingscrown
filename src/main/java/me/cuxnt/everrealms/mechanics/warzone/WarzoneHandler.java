package me.cuxnt.everrealms.mechanics.warzone;

import me.cuxnt.everrealms.KingsCrown;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.time.LocalDate;
import java.util.Random;

public class WarzoneHandler {

    public static int radius = 120;

    public static boolean contains(Location player_location) {
        //Use distance formula to check if a specific point is within our circle.
        double distance = Math.sqrt(Math.pow(Warzone.warzone_origin.getX() - player_location.getX(), 2) + (Math.pow(Warzone.warzone_origin.getZ() - player_location.getZ(), 2)));
        if (distance  <= radius * 2)
            return true;
        else {
            return false;
        }
    }

    public static double getDistanceFromOrigin(Location location){
        double distance = Math.sqrt(Math.pow(Warzone.warzone_origin.getX() - location.getX(), 2) + (Math.pow(Warzone.warzone_origin.getZ() - location.getZ(), 2)));
        return distance;
    }

    public static Location randomLocation(){

        //Create Random Location withtin radius
        Random random = new Random();
        Location random_location = new Location(KingsCrown.world, random.nextInt(radius) - random.nextInt(radius),
                100, random.nextInt(radius) - random.nextInt(radius));

        //Check if random location is in within radius
        if(getDistanceFromOrigin(random_location) > radius){
            //Restart operation until inside radius
            randomLocation();
        }

        return random_location;
    }
}
