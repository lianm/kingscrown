package me.cuxnt.everrealms.mechanics.warzone;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.warzone.join.WarzoneJoinListener;
import me.cuxnt.everrealms.utils.logger.Console;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class WarzoneListener implements Listener {

    private WarzoneJoinListener warzoneJoinListener;

    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
        warzoneJoinListener = new WarzoneJoinListener();
        warzoneJoinListener.onEnable();
        Console.log(this.getClass(), true);
    }

    @EventHandler
    void playerDeath(PlayerDeathEvent event){
        Player player_dead = event.getEntity().getPlayer();
        Player player_killer = event.getEntity().getKiller();
        Warzone.playerKill(player_dead, player_killer);
    }

    @EventHandler
    void quitEvent(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if(Warzone.playerInWarzone.containsKey(player)){
            Warzone.quit(player);
        }
    }

}
