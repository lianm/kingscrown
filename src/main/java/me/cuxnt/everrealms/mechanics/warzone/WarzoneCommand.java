package me.cuxnt.everrealms.mechanics.warzone;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarzoneCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player && !sender.isOp()) {
            Player player = (Player) sender;
            String action = args[0];
            if (action == "join") {
                Warzone.join(player);
            }else if(action == "quit" || action == "leave"){
                Warzone.quit(player);
            }
        }
        return false;
    }
}
