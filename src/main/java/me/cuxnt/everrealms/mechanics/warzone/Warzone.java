package me.cuxnt.everrealms.mechanics.warzone;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class Warzone {

    public static HashMap<Player, PlayerStats> playerInWarzone = new HashMap<>();

    public static final Location warzone_origin = new Location(KingsCrown.world, 0, 0, 0);

    public static void join(Player player){
        announceMsg(player, "has joined");
        player.teleport(WarzoneHandler.randomLocation());
        playerInWarzone.put(player, new PlayerStats(player));
    }

    public static void announceMsg(Player player, String message){
        Bukkit.broadcastMessage(ChatColor.RED + "Warzone - " + ChatColor.WHITE + player.getName() + " " + message);
    }

    public static void playerKill(Player player_killed, Player player_killer){
        PlayerStats.playerdata.get(player_killer).getCombatStats().setKills(1);
        PlayerStats.playerdata.get(player_killed).getCombatStats().setDeaths(1);
        quit(player_killed);
    }

    public static void quit(Player player){
        announceMsg(player, "has been eliminated");
        playerInWarzone.remove(player);
    }
}
