package me.cuxnt.everrealms.mechanics.warzone.join;

import me.cuxnt.everrealms.KingsCrown;
import me.cuxnt.everrealms.mechanics.warzone.Warzone;
import me.cuxnt.everrealms.mechanics.warzone.WarzoneHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.HashMap;

public class WarzoneJoinListener implements Listener {

    public static HashMap<Player, Boolean> inAction = new HashMap<>();

    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(this, KingsCrown.plugin);
    }

    @EventHandler
    public void onTeleportClick(PlayerInteractEntityEvent event){
        event.setCancelled(true);
        HumanEntity npc = (HumanEntity) event.getRightClicked();
        Player player = event.getPlayer();
        if(npc.getName().equals("Teleporter")){
            if(!inAction.containsKey(player)){
                inAction.put(player, true);
                player.sendMessage(ChatColor.GREEN + "Click again to confirm");
                player.sendMessage(ChatColor.GRAY + "Request will time out in 5 seconds...");
                Bukkit.getScheduler().runTaskLater(KingsCrown.plugin, new Runnable() {

                    @Override
                    public void run() {
                        if(inAction.containsKey(player)){
                            inAction.remove(player);
                            player.sendMessage(ChatColor.GRAY + "Join request timed out.");
                        }
                    }
                }, 100);
            }else{
                inAction.remove(player);
                Warzone.join(player);
            }
            player.sendMessage("Teleporter clicked");

        }
    }
}
