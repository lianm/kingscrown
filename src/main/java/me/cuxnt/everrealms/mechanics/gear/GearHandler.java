package me.cuxnt.everrealms.mechanics.gear;

import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class GearHandler {

    public static String nameFactory(Gear gear){
        String display_name = gear.getTier().getPrefix() + " " + gear.getItem().getTitle();
        if(gear.getStats().getEnchants() != null) {
            for (GearEnchant ench : gear.getStats().getEnchants()) {
                String line = ench.toString();
                if (ench == GearEnchant.PURE_DMG) display_name = "Pure " + display_name;
                if (ench == GearEnchant.ACCURACY) display_name = "Accurate " + display_name;
                if (ench == GearEnchant.LIFE_STEAL) display_name = "Vampyric " + display_name;
                if (ench == GearEnchant.CRITICAL_HIT) display_name = "Deadly " + display_name;
                if (ench == GearEnchant.ICE_DMG) display_name +=" of Ice";
                if (ench == GearEnchant.POISON_DMG) display_name +=" of Poison";
                if (ench == GearEnchant.FIRE_DMG) display_name +=" of Fire";
            }
        }
        return gear.getTier().getColor() + display_name;
    }

    public static List<Gear> getGearList(Player p){
        List<Gear> gear_list = new ArrayList<>();
        for(ItemStack item : p.getInventory().getArmorContents()) {
            if (item.hasItemMeta()) {
                gear_list.add(new Gear(item));
            }
        }
        return gear_list;
    }


}
