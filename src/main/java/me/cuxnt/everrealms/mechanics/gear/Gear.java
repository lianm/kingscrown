package me.cuxnt.everrealms.mechanics.gear;

import me.cuxnt.everrealms.mechanics.gear.stats.Stats;
import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import me.cuxnt.everrealms.mechanics.tiers.TierHandler;
import me.cuxnt.everrealms.utils.JsonHandler;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.swing.plaf.synth.SynthScrollBarUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Gear {

    private String display_name;
    private Rarity rarity;
    private Tier tier;
    private Item item;
    private GearType gearType;
    private Material material;
    private Stats stats;
    private String json;

    public Gear() {
        super();
        this.tier = Tier.ONE;
        this.rarity = Rarity.LEGENDARY;
        this.item = Item.AXE;
        this.stats = new Stats(this);
        this.display_name = GearHandler.nameFactory(this);
        this.json = JsonHandler.toJSON(this);
    }

    public Gear(Tier tier, Item item, Rarity rarity) {
        System.out.print(tier.name() + item.name() + rarity.name());
        this.tier = tier;
        this.rarity = rarity;
        this.item = item;
        this.gearType = item.getGearType();
        this.stats = new Stats(this);
        this.display_name = GearHandler.nameFactory(this);
        this.material = Material.getMaterial(tier.getMaterialPrefix(item) + "_"  + item.name());
        this.json = JsonHandler.toJSON(this);
    }

    public Gear(ItemStack itemStack){
        this.rarity = Rarity.getRarity(itemStack);
        this.material = itemStack.getType();
        this.tier = Tier.values()[TierHandler.getItemTier(itemStack)];
        this.item = Item.getItem(itemStack);
        this.gearType = item.getGearType();
        this.stats = new Stats(itemStack);
        this.display_name = GearHandler.nameFactory(this);
        this.json = JsonHandler.toJSON(this);
    }

    public String getJson() {
        return json;
    }

    public Material getMaterial() {
        return material;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public Stats getStats() {
        return stats;
    }

    public Item getItem() {
        return item;
    }

    public Tier getTier() {
        return tier;
    }

    public GearType getItemType() {
        return gearType;
    }

    //The only 3 factors that should be changed when creating gear
    //Is Tier, Item, and Rarity.

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public void setTier(Tier tier) {
        this.tier = tier;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setName(String display_name) {
        this.display_name = display_name;
    }

    public List buildLore(){

        List lore = new ArrayList();
        //Depending on the item type, add the base stats
        if (getItemType().equals(GearType.WEAPON)) {
            lore.add(ChatColor.RED + "DMG: " + getStats().getDamage()[0] + " - " + getStats().getDamage()[1]);
        } else {
            lore.add(ChatColor.RED + "HP: +" + getStats().getHp());
            lore.add(ChatColor.RED + "ARMOR: " + getStats().getArmor() + "% - " + getStats().getArmor() + "%");
            if(new Random().nextInt(2) == 0){
                lore.add(ChatColor.RED + "HP REGEN: +" + getStats().getHpregen() + " HP/s");
            }else {
                lore.add(ChatColor.RED + "ENERGY REGEN: " + getStats().getEnergy() + "%");
            }
        }

        //Add Enchants
        for(GearEnchant enchant : stats.getEnchants()){
            lore.add(enchant.toLine(tier));
        }

        //Add rarity
        lore.add(rarity.getTitle());

        //Complete
        return lore;
    }

    //Builds item
    public ItemStack build() {
        //Initialize Variables
        ItemStack drop = new ItemStack(material);
        ItemMeta meta = drop.getItemMeta();
        meta.setDisplayName(display_name);
        meta.setLore(this.buildLore());
        drop.setItemMeta(meta);
        return drop;
    }
}
