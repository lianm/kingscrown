package me.cuxnt.everrealms.mechanics.gear;

import me.cuxnt.everrealms.mechanics.gear.Item;
import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GearCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (sender instanceof Player && sender.isOp()) {

            Player p = (Player) sender;
            ItemStack hand = p.getInventory().getItemInMainHand();

                if (args[0].contains("create")) {
                    Tier tier = Tier.values()[Integer.parseInt(args[1]) - 1];
                    Item item = Item.values()[Integer.parseInt(args[2])];
                    Rarity rarity = Rarity.values()[Integer.parseInt(args[3])];
                    Gear gear = new Gear(tier, item, rarity);
                    p.getInventory().addItem(gear.build());
                }
                if (args[0].contains("info")) {
                    if (Item.isItem(p.getInventory().getItemInMainHand())) {
                        Gear gear_hand = new Gear(hand);
                        p.sendMessage(gear_hand.toString());
                    } else {
                        p.sendMessage("This is not a piece of equipment!!");
                    }
                }else{
                    p.sendMessage(ChatColor.RED + "/gear create 1 2 0 >> Creates T1 Common Sword");
                    p.sendMessage(ChatColor.RED + "/gear info >> Display gear info in JSON");
                    p.sendMessage(ChatColor.RED + "/gear set >> Change item variables");
                    p.sendMessage(ChatColor.RED + "/gear set name>> Change item name");
                }

        }
        return false;
    }
}

