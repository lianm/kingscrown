package me.cuxnt.everrealms.mechanics.gear.stats;

import me.cuxnt.everrealms.mechanics.rarity.Rarity;
import me.cuxnt.everrealms.mechanics.tiers.Tier;

import java.util.Random;

public class StatFactory {

    public static int setArmor(int t){
        return (int) (-0.05D*(t*t) + (3.5D*t));
    }

    public static int setAccuracy(int tier){
        return (int) ((6*tier) +4);
    }

    public static int setEnergy(int t){
        return new Random().nextInt(3) + t +1;
    }

    public static int setDodge(int t){
        return (int) ((-0.01D*(t*t)+(0.25D*t) +0.2D)*10D);
    }

    public static int setVit(int t){
        return (int) ((0.1D*(t*t)+(0.01D*t)+0.05D)*100D);
    }

    public static int setElement(int t){
        return (int) (2.4 * (t*t))+4;
    }

    public static int setLifeSteal(int t){
        return (int) ((13 - t) * 1.8D);
    }

    public static int setVs(int t){
       return (int) ((2D*t)+2.5D);
    }

    private static int[][] health_data = {
            {10, 13, 2},
            {85, 25, 5},
            {235, 85, 10},
            {700, 200, 50},
            {1950, 450, 50}
    };

    private static int[][] hps_data = {
            {5, 7, 0},
            {28, 9, 1},
            {59, 21, 1},
            {140, 40, 10},
            {325, 75, 8}
    };
    //3, Common =
    private static int[] ranges = {3, 5, 10, 15, 27};
    private static int[] bases = {0, 14, 35, 83, 191};

    public static int[] getDamageValue(int tier, int rarity) {
        System.out.print(tier + " " + rarity);
        int[] min_damage = doMulti(tier, rarity);
        int[] max_damage = doMulti(tier, rarity+1);
        int[] damage = {0,0};
        damage[0] = getRandomIntFromRange(min_damage) +1;
        damage[1] = getRandomIntFromRange(max_damage) +1;
        return damage;
    }

    public static int getRandomIntFromRange(int[] number){
        Random random = new Random();
        return random.nextInt(number[1] - number[0]) + number[0];
    }

    private static int[] doMulti(int index, int range){
        int base = bases[index] + 1;
        int result[] = {base, bases[index] + ranges[index]};

        for(int i = 0; i < range; i++){
            result[0] = result[1];
            result[1] = result[0] + ranges[index];
        }
        return result;
    }

    public static int translateDataPattern(int tier, int rarity, int[][] data){
        int base = data[tier][0];
        int range = data[tier][1];
        int gap = data[tier][2];
        int[] output_range = {base, base+range};
        for(int i = 0; i < rarity; i++){
            output_range[0] = output_range[1] + gap;
            output_range[1] = output_range[0] + range;
        }
        Random random = new Random();
        int output_data = random.nextInt(output_range[1] - output_range[0]) + output_range[0];
        return output_data;
    }

    public static int getHealth(Tier tier, Rarity rarity){
        System.out.println(tier.toString() + " " + rarity.getTitle());
        return translateDataPattern(tier.index(), rarity.toInt(), health_data);
    }

    public static int getHpRegen(Tier tier, Rarity rarity) {
        return translateDataPattern(tier.index(), rarity.toInt(), hps_data);
    }

}
