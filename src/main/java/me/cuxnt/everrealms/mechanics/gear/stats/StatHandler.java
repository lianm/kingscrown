package me.cuxnt.everrealms.mechanics.gear.stats;

import me.cuxnt.everrealms.mechanics.gear.GearEnchant;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class StatHandler {

    public static int getHp(ItemStack is) {
        List<String> lore;
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 1 && lore.get(1).contains("HP")) {
            try {
                return Integer.parseInt(lore.get(1).split(": +")[1]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public static int getArmor(ItemStack is) {
        List<String> lore;
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 0 && lore.get(0).contains("ARMOR")) {
            try {
                return Integer.parseInt(lore.get(0).split(" - ")[1].split("%")[0]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public static int getDps(ItemStack is) {
        List<String> lore;
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 0 && lore.get(0).contains("DPS")) {
            try {
                return Integer.parseInt(lore.get(0).split(" - ")[1].split("%")[0]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public static int getEnergy(ItemStack is) {
        List<String> lore;
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 2 && lore.get(2).contains("ENERGY REGEN")) {
            try {
                return Integer.parseInt(lore.get(2).split(": +")[1].split("%")[0]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public static int getHps(ItemStack is) {
        List<String> lore;
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 2 && lore.get(2).contains("HP REGEN")) {
            try {
                return Integer.parseInt(lore.get(2).split(": +")[1].split(" HP/s")[0]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public static int getPercent(ItemStack is, String type) {
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()) {
            List<String> lore = is.getItemMeta().getLore();
            for (String s : lore) {
                if (!s.contains(type))
                    continue;
                try {
                    return Integer.parseInt(s.split(": ")[1].split("%")[0]);
                } catch (Exception e) {
                    return 0;
                }
            }
        }
        return 0;
    }

    public static int getElem(ItemStack is, String type) {
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()) {
            List<String> lore = is.getItemMeta().getLore();
            for (String s : lore) {
                if (!s.contains(type))
                    continue;
                try {
                    return Integer.parseInt(s.split(": +")[1]);
                } catch (Exception e) {
                    return 0;
                }
            }
        }
        return 0;
    }

    public static List<GearEnchant> getGearEnchants(ItemStack item){
        List<GearEnchant> enchant_list = new ArrayList<>();
        List<String> lore = item.getItemMeta().getLore();
        for (String line : lore){
            for(GearEnchant enchants : GearEnchant.values()){
                if(line.contains(enchants.getName())){
                   enchant_list.add(enchants);
                }
            }
        }
        return enchant_list;
    }

    public static int[] getDamageRange(ItemStack is) {
        List<String> lore;
        int[] dmg = {0, 0};
        if (is != null && is.getType() != Material.AIR && is.getItemMeta().hasLore()
                && (lore = is.getItemMeta().getLore()).size() > 0 && lore.get(0).contains("DMG")) {
            try {
                int min = Integer.parseInt(lore.get(0).split("DMG: ")[1].split(" - ")[0]);
                int max = Integer.parseInt(lore.get(0).split(" - ")[1]);
                dmg[0] = min;
                dmg[1] = max;
            } catch (Exception e) {
                dmg[0] = 1;
                dmg[1] = 1;
            }
        }
        return dmg;
    }

    public static int getCrit(Player p) {
        int crit = 0;
        ItemStack wep = p.getInventory().getItemInMainHand();
        if (wep != null && wep.getType() != Material.AIR && wep.getItemMeta().hasLore()) {
            List<String> lore = wep.getItemMeta().getLore();
            for (String line : lore) {
                if (!line.contains("CRITICAL HIT"))
                    continue;
                crit = getPercent(wep, "CRITICAL HIT");
            }
            if (wep.getType().name().contains("_AXE")) {
                crit += 10;
            }
            int intel = 0;
            ItemStack[] arritemStack = p.getInventory().getArmorContents();
            int n = arritemStack.length;
            int n2 = 0;
            while (n2 < n) {
                ItemStack is = arritemStack[n2];
                if (is != null && is.getType() != Material.AIR && is.hasItemMeta() && is.getItemMeta().hasLore()) {
                    int addint = getElem(is, "INT");
                    intel += addint;
                }
                ++n2;
            }
            if (intel > 0) {
                crit = (int) ((long) crit + Math.round((double) intel * 0.015));
            }
        }
        return crit;
    }
}
