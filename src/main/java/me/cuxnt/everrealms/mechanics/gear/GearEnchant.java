package me.cuxnt.everrealms.mechanics.gear;

import me.cuxnt.everrealms.mechanics.gear.stats.StatFactory;
import me.cuxnt.everrealms.mechanics.tiers.Tier;
import me.cuxnt.everrealms.utils.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum GearEnchant {

    LIFE_STEAL(GearType.WEAPON),
    POISON_DMG(GearType.WEAPON),
    PURE_DMG(GearType.WEAPON),
    CRITICAL_HIT(GearType.WEAPON),
    ICE_DMG(GearType.WEAPON),
    FIRE_DMG(GearType.WEAPON),
    VS_MONSTERS(GearType.WEAPON),
    VS_PLAYERS(GearType.WEAPON),
    ACCURACY(GearType.WEAPON),
    VIT(GearType.ARMOR),
    DEX(GearType.ARMOR),
    INT(GearType.ARMOR),
    STR(GearType.ARMOR),
    BLOCK(GearType.ARMOR),
    DODGE(GearType.ARMOR);

    private String title;
    private String name;
    private String format;
    private GearType gearType;

    GearEnchant(GearType type) {
        this.title = ChatColor.RED + StringUtils.capitalise(this.toString().toLowerCase()).replace("_", " ");
        this.name = this.name().replace("_", " ");
        this.gearType = type;
    }

    public static boolean ifElementalExists(List<GearEnchant> list, GearEnchant added_enchant){
        for(GearEnchant enchant : list){
            if(isElemental(added_enchant)) return true;
        }
        return false;
    }

    public static boolean isElemental(GearEnchant enchant){
        switch (enchant) {
            case FIRE_DMG:
            case ICE_DMG:
            case POISON_DMG:
                return true;
        }
        return false;
    }

    public String toLine(Tier tier) {
        int t = tier.index() + 1;
        String format = "%1$s";
        String line = ChatColor.RED + this.name + ": ";
        int value = 0;
        switch (this) {
            case ACCURACY:
                value = new Random().nextInt(StatFactory.setAccuracy(t)) + 1;
                return line + String.format(format, value) + "%";
            case DEX:
            case STR:
            case INT:
            case VIT:
                value = new Random().nextInt(StatFactory.setVit(t)) + 1;
                return line + "+" + String.format(format, value);
            case BLOCK:
            case DODGE:
                value = new Random().nextInt(StatFactory.setDodge(t)) + 1;
                return line + String.format(format, value) + "%";
            case FIRE_DMG:
            case PURE_DMG:
            case POISON_DMG:
            case ICE_DMG:
                value = new Random().nextInt(StatFactory.setElement(t)) + 1;
                return line + "+" + String.format(format, value);
            case CRITICAL_HIT:
            case VS_MONSTERS:
            case VS_PLAYERS:
                value = new Random().nextInt(StatFactory.setVs(t)) + 1;
                return line + String.format(format, value) + "%";
            case LIFE_STEAL:
                value = new Random().nextInt(StatFactory.setLifeSteal(t))+ 1;
                return line + String.format(format, value) + "%";

        }
        value = new Random().nextInt(value) + 1;
        line = line + String.format(format, value);
        return ChatColor.RED + this.name + line;
    }

    public GearType getType() {
        return gearType;
    }

    public static int getValue(String line) {
        return StringUtil.getIntegerFromString(line);
    }

    public String getName() {
        return name;
    }

    //Create a generated lsit of enchants for when items are orbed.
    public static List<GearEnchant> createEnchList(Item item) {
        GearType gearType = item.getGearType();
        List<GearEnchant> enchList = new ArrayList<>();
        for(GearEnchant ench : GearEnchant.values()){
            System.out.print(enchList);
            if(!(ench.getType()).equals(gearType))
                continue;
            if(ench.equals(ACCURACY) && !item.equals(Item.SWORD)) continue;
            if(ench.equals(PURE_DMG) && !item.equals(Item.AXE)) continue;
            if(new Random().nextInt(10) > 7) {
                if (enchList.size() == 0) enchList.add(ench);
                else if (!ifElementalExists(enchList, ench)) enchList.add(ench);
            }
        }
        return enchList;
    }
}
