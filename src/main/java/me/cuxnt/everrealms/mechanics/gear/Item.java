package me.cuxnt.everrealms.mechanics.gear;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum Item {

    HOE(GearType.WEAPON),
    SPADE(GearType.WEAPON),
    SWORD(GearType.WEAPON),
    AXE(GearType.WEAPON),
    HELMET(GearType.ARMOR),
    CHESTPLATE(GearType.ARMOR),
    LEGGINGS(GearType.ARMOR),
    BOOTS(GearType.ARMOR);

    private int id;
    private GearType gearType;
    private String title;

    Item(GearType gearType){
        this.title = StringUtils.capitalise(this.toString().toLowerCase());
        this.gearType = gearType;
        this.id = this.ordinal();
    }

    public static Item getItemFromStack(ItemStack itemStack){
        String material_item = itemStack.getType().toString().split("_")[1];
        return Item.valueOf(material_item);
    }

    public int toInt() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public GearType getGearType() {
        return gearType;
    }

    public static boolean isItem(ItemStack itemStack){
        if(itemStack.getType() == Material.AIR && itemStack.hasItemMeta()) return false;
        for (Item item : Item.values()){
            if(itemStack.getType().toString().contains(item.getTitle())){
                return true;
            }
        }
        return false;
    }

    public static Item getItem(ItemStack itemStack) {
        String material_string = itemStack.getType().toString().split("_")[1];
        return Item.valueOf(material_string);
    }

}
