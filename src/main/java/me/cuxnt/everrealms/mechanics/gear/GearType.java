package me.cuxnt.everrealms.mechanics.gear;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public enum GearType {

    ARMOR,
    WEAPON;

    public String type;

    GearType(){
        this.type = this.toString().toLowerCase();
    }

    public static GearType getItemType(ItemStack is){
        String itemName = ChatColor.stripColor(is.getItemMeta().getDisplayName());
        for(GearType itemType : GearType.values()){
            if(itemName.toLowerCase().contains(itemType.type)){
                return itemType;
            }
        }
        return null;
    }

}
