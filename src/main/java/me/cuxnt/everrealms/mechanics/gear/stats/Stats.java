package me.cuxnt.everrealms.mechanics.gear.stats;

import me.cuxnt.everrealms.mechanics.gear.Gear;
import me.cuxnt.everrealms.mechanics.gear.GearEnchant;
import me.cuxnt.everrealms.mechanics.gear.GearType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class Stats {
    private int energy;
    private int hpregen;
    private static int hp;
    private int[] damage;
    private int armor;
    private List<GearEnchant> enchants;

    public Stats(Gear gear){
      if(gear.getItemType() == GearType.ARMOR) {
         this.energy = StatFactory.setEnergy(gear.getTier().index());
         this.hp = StatFactory.getHealth(gear.getTier(), gear.getRarity());
         this.hpregen = StatFactory.getHpRegen(gear.getTier(), gear.getRarity());
         this.armor = StatFactory.setArmor(gear.getTier().index());
      }else{
          this.damage = StatFactory.getDamageValue(gear.getTier().index(), gear.getRarity().toInt());
      }
      this.enchants = GearEnchant.createEnchList(gear.getItem());
    }

    public Stats(ItemStack item){
        this.hp = StatHandler.getHp(item);
        this.energy = StatHandler.getEnergy(item);
        this.damage = StatHandler.getDamageRange(item);
        this.enchants = StatHandler.getGearEnchants(item);
        this.armor = StatHandler.getArmor(item);
        this.hpregen = StatHandler.getHps(item);
    }

    public int getHpregen() {
        return hpregen;
    }

    public int getEnergy() {
        return energy;
    }

    public int getArmor() {
        return armor;
    }

    public int getHp() {
        return hp;
    }

    public List<GearEnchant> getEnchants() {
        return enchants;
    }

    public int[] getDamage() {
        return damage;
    }

    public void setHpregen(int hpregen) {
        this.hpregen = hpregen;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setEnchants(List<GearEnchant> enchants) {
        this.enchants = enchants;
    }


}
