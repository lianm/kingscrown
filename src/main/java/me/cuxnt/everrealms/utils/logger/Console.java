package me.cuxnt.everrealms.utils.logger;

import java.util.logging.Logger;

public class Console {

    public final static Logger logger = Logger.getLogger("Minecraft");

    public static void log(Class className, boolean enabled){
        if(enabled) logger.info(className.getSimpleName() + "=> Enabled!");
        else logger.info(className.getSimpleName() + "=> Disabled!");
    }

}
