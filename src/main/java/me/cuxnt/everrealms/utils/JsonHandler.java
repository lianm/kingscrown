package me.cuxnt.everrealms.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonHandler {
    public static String toJSON(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(obj);
            return json;
            //System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
