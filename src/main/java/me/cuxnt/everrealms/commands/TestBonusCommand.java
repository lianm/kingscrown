package me.cuxnt.everrealms.commands;

import me.cuxnt.everrealms.mechanics.player.data.PlayerStats;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestBonusCommand  implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player){
            Player p = (Player)sender;
            PlayerStats.sync(p);
            p.sendMessage(p.getLocation().getWorld().getHighestBlockYAt(p.getLocation()) + " - highest Y block");
        }
        return false;
    }
}