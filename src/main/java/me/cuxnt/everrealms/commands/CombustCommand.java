package me.cuxnt.everrealms.commands;

import me.cuxnt.everrealms.mechanics.combust.Combust;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CombustCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        Player p = (Player)sender;
        if(sender instanceof Player){
            Combust.openCombustionGUI(p);
        }
        return false;
    }
}
