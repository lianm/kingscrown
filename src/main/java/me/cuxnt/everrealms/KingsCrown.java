package me.cuxnt.everrealms;

import me.cuxnt.everrealms.commands.*;
import me.cuxnt.everrealms.mechanics.abilities.DoubleJump;
import me.cuxnt.everrealms.mechanics.combust.Combust;
import me.cuxnt.everrealms.mechanics.crafting.CraftingListener;
import me.cuxnt.everrealms.mechanics.damage.Damage;
import me.cuxnt.everrealms.mechanics.gear.GearCommand;
import me.cuxnt.everrealms.mechanics.healthbar.HealthBarListener;
import me.cuxnt.everrealms.mechanics.kits.KitHandler;
import me.cuxnt.everrealms.mechanics.lootcrate.LootCrate;
import me.cuxnt.everrealms.mechanics.mining.Mining;
import me.cuxnt.everrealms.mechanics.mining.MiningListener;
import me.cuxnt.everrealms.mechanics.player.PlayerListener;
import me.cuxnt.everrealms.mechanics.player.PlayerCommand;
import me.cuxnt.everrealms.mechanics.warzone.WarzoneCommand;
import me.cuxnt.everrealms.mechanics.warzone.WarzoneListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class KingsCrown extends JavaPlugin {

    private Mining mining;
    private KitHandler kitHandler;
    private MiningListener miningListener;
    private DoubleJump doubleJump;
    private LootCrate lootCrate;
    private CraftingListener craftingListener;
    private WarzoneListener warzoneListener;
    private Combust combust;
    private PlayerListener playerListener;
    private Damage damage;
    private HealthBarListener healthBarListener;
    public static KingsCrown plugin;
    public final static World world = Bukkit.getWorld("drmap");
    public final static String version = "1.0 BETA";
    public final static Location spawn = new Location(world, -146.5, 100, 123.5);

    public void onEnable() {
        plugin = this;
//        db.connect();
        //Initiallise
        mining = new Mining();
        miningListener = new MiningListener();
        lootCrate = new LootCrate();
        doubleJump = new DoubleJump();
        healthBarListener = new HealthBarListener();
        kitHandler = new KitHandler();
        craftingListener = new CraftingListener();
        warzoneListener = new WarzoneListener();
        playerListener = new PlayerListener();
        damage = new Damage();
        combust = new Combust();

        //On Enable
        warzoneListener.onEnable();
        mining.onEnable();
        kitHandler.onEnable();
        //doubleJump.onEnable();
        //lootCrate.onEnable();
        playerListener.onEnable();
        craftingListener.onEnable();
        healthBarListener.onEnable();
        damage.onEnable();
        combust.onEnable();
        miningListener.onEnable();

        registerCommands();
    }

    public void onDisable(){
        miningListener.onDisable();
        playerListener.onDisbale();
        lootCrate.onDisable();
    }


    public void registerCommands(){
        getCommand("combust").setExecutor(new CombustCommand());
        getCommand("player").setExecutor(new PlayerCommand());
        getCommand("warzone").setExecutor(new WarzoneCommand());
        getCommand("test").setExecutor(new TestBonusCommand());
        getCommand("gear").setExecutor(new GearCommand());
    }
}
